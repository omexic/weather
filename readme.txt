#Assumptions

1. Data whose MxT or MnT denoted by '*' is faulty and therefore not included in the computation
2. Data format is not subject to change
3. Data file location is constant
3. Python is already installed
4. Day with 'mo' is month summary and should not be included in finding maximum spread


#How to run the code

1. Unzip Antony 'Omeri - Technical Assessment – KENYA.zip' to a suitable location
2. Navigate to the home directory of the solution on Terminal
3. Run: 'python weather.py'



