#!/usr/bin/python
import sys
import re

"""
Author: Omeri Antony
Created: Sunday, 19, 2017
"""
class WeatherSpread:
    def __init__(self, file_name):
        self._file_name = file_name

    def __read_file(self):
        """
        Read all file data to the memory, and remove the first two rows
        """
        try:
            return [data.strip().split() for data in open(self._file_name).readlines()[2:]]
        except Exception as error:
            raise Exception(error.message)

    def __is_numeric(self, inputString):
        """
        Use regular expression to get check if a string value is numeric
        """
        return bool(re.search(r'^[-+]?[0-9]+(\.[0-9]?)?$', inputString))

    def __filter_data_row(self, data_row):
        """
        Check the first three data columns pass sanity checks
        """
        return self.__is_numeric(data_row[0]) and self.__is_numeric(data_row[1]) and self.__is_numeric(data_row[2])

    def get_max_spread(self):
        max_spread = ''
        try:
            """Remove remove days with non numeric minimum values using filter fn"""
            data =  filter((lambda x: self.__filter_data_row(x)), self.__read_file())
            day = '0'
            max_spread_value = 0
            for dayData in data:
                day = dayData[0]
                max_temp = int(dayData[1])
                min_temp = int(dayData[2])
                """Calculate the spread (max_temp-min_temp)"""
                spread = max_temp - min_temp
                """Get maximum spread"""
                max_spread_value = max(max_spread_value, spread)
            max_spread = '{0} {1}'.format(day, max_spread_value)
        except Exception as error:
            raise Exception(error.message)
        return max_spread;

if __name__ == "__main__":
    file_name = 'weather.dat'
    weatherSpread = WeatherSpread(file_name)
    max_spread = weatherSpread.get_max_spread()
    print max_spread

